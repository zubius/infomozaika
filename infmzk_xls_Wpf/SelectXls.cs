﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NLog;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Select data from Excel
    /// </summary>
    class SelectXls
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        //Select data while notlike text box is filled. Returns empty data if notlike is empty
        private Dictionary<string, List<SelectedData>> Select(Dictionary<string, DataTable> data, int maxCost, string like, string notlike)
        {
            Dictionary<string, List<SelectedData>> datas = new Dictionary<string, List<SelectedData>>();
            List<SelectedData> selData;
            List<SelectedData> preSelData;
            List<SelectedData> finData = new List<SelectedData>();

            foreach (KeyValuePair<string, DataTable> kvp in data)
            {
                try
                {
                    if (kvp.Value.Columns.Contains("G45 (Таможенная стоимость)"))
                    {
                        if (kvp.Value.Columns.Contains("Доля на рынке"))
                        {
                            finData = (from row in kvp.Value.AsEnumerable()
                                       where (row.Field<string>("G31_1 (Описание и характеристика товара)") != null)
                                       && row.Field<string>("G31_1 (Описание и характеристика товара)").Contains(like)
                                       && !row.Field<string>("G31_1 (Описание и характеристика товара)").Contains(notlike)
                                       select new SelectedData
                                       {
                                           Company = row.Field<string>("G082 (Получатель)"),
                                           INN = row.Field<double>("G081 (ИНН получателя)"),
                                           Descr = row.Field<string>("G31_1 (Описание и характеристика товара)"),
                                           Manuf = row.Field<string>("G31_11 (Фирма изготовитель)"),
                                           Cost = row.Field<double>("G45 (Таможенная стоимость)"),
                                           Total = row.Field<double>("Объем рынка"),
                                           Percent = row.Field<string>("Доля на рынке")
                                       }).ToList();

                            foreach (var dat in finData)
                            {
                                dat.Fields = new object[] { dat.Company, dat.INN, dat.Descr, dat.Manuf, dat.Cost, dat.Total, dat.Percent };
                            }
                        }
                        else
                        {
                            var total = kvp.Value.AsEnumerable().Sum(t => t.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : t.Field<double?>("G45 (Таможенная стоимость)"));

                            preSelData = (from d in kvp.Value.AsEnumerable()
                                          group d by new
                                              {
                                                  rec = d.Field<string>("G082 (Получатель)"),
                                                  inn = (d.Field<double?>("G081 (ИНН получателя)") == null ? 0 : d.Field<double>("G081 (ИНН получателя)")),
                                                  manuf = d.Field<string>("G31_11 (Фирма изготовитель)"),
                                                  charact = d.Field<string>("G31_1 (Описание и характеристика товара)")
                                              } into mg
                                          where (mg.Key.charact != null)
                                          && mg.Key.charact.Contains(like)
                                          && !mg.Key.charact.Contains(notlike)

                                          select new SelectedData
                                         {
                                             Company = mg.Key.rec,
                                             INN = mg.Key.inn,
                                             Descr = mg.Key.charact,
                                             Manuf = mg.Key.manuf,
                                             Cost = Convert.ToDouble(mg.Sum(x => x.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : x.Field<double?>("G45 (Таможенная стоимость)")))
                                         }).ToList();

                            selData = (from s in preSelData
                                       group s by new
                                       {
                                           comp = s.Company
                                       } into sg
                                       where ((sg.Sum(x => x.Cost) / total) * 100) > maxCost
                                       select new SelectedData
                                       {
                                           Cost = sg.Sum(x => x.Cost),
                                           Company = sg.Key.comp
                                       }).ToList();

                            foreach (var predat in preSelData)
                            {
                                foreach (var dat in selData)
                                {
                                    if (predat.Company.Equals(dat.Company))
                                    {
                                        predat.Cost = dat.Cost;
                                        predat.Total = Convert.ToDouble(total);
                                        predat.Percent = String.Format("{0:0.##}", (predat.Cost / total) * 100) + "%";
                                        finData.Add(predat);
                                    }
                                }
                            }
                        }
                        datas.Add(kvp.Key, finData);
                    }
                }
                catch (System.ArgumentException aex)
                {
                    log.Error("Table missformat \r\n {0}", aex.ToString());
                }
                catch (System.FormatException fex)
                {
                    log.Error("Failed to format double data from LINQ \r\n {0}", fex.ToString());
                }
                catch (System.InvalidCastException icex)
                {
                    log.Error("Failed to cast double data from LINQ \r\n {0}", icex.ToString());
                }
                finally { }
            }
            return datas;
        }

        //Select data while notlike text box is not filled
        //TODO! Change to DataTable?
        private Dictionary<string, List<SelectedData>> Select1(Dictionary<string, DataTable> data, int maxCost, string like)
        {
            Dictionary<string, List<SelectedData>> datas = new Dictionary<string, List<SelectedData>>();
            List<SelectedData> selData;
            List<SelectedData> preSelData;
            List<SelectedData> finData = new List<SelectedData>();

            foreach (KeyValuePair<string, DataTable> kvp in data)
            {
                try
                {
                    if (kvp.Value.Columns.Contains("G45 (Таможенная стоимость)"))
                    {
                        if (kvp.Value.Columns.Contains("Доля на рынке"))
                        {
                            finData = (from row in kvp.Value.AsEnumerable()
                                       where (row.Field<string>("G31_1 (Описание и характеристика товара)") != null)
                                       && row.Field<string>("G31_1 (Описание и характеристика товара)").Contains(like)
                                       select new SelectedData
                                        {
                                            Company = row.Field<string>("G082 (Получатель)"),
                                            INN = row.Field<double>("G081 (ИНН получателя)"),
                                            Descr = row.Field<string>("G31_1 (Описание и характеристика товара)"),
                                            Manuf = row.Field<string>("G31_11 (Фирма изготовитель)"),
                                            Cost = row.Field<double>("G45 (Таможенная стоимость)"),
                                            Total = row.Field<double>("Объем рынка"),
                                            Percent = row.Field<string>("Доля на рынке")
                                        }).ToList();

                            foreach (var dat in finData)
                            {
                                dat.Fields = new object[] { dat.Company, dat.INN, dat.Descr, dat.Manuf, dat.Cost, dat.Total, dat.Percent };
                            }
                        }
                        else
                        {
                            var total = kvp.Value.AsEnumerable().Sum(t => t.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : t.Field<double?>("G45 (Таможенная стоимость)"));

                            preSelData = (from d in kvp.Value.AsEnumerable()
                                          group d by new
                                          {
                                              rec = d.Field<string>("G082 (Получатель)"),
                                              inn = (d.Field<double?>("G081 (ИНН получателя)") == null ? 0 : d.Field<double>("G081 (ИНН получателя)")),
                                              manuf = d.Field<string>("G31_11 (Фирма изготовитель)"),
                                              charact = d.Field<string>("G31_1 (Описание и характеристика товара)")
                                          } into mg
                                          where (mg.Key.charact != null)
                                          && mg.Key.charact.Contains(like)

                                          select new SelectedData
                                          {
                                              Company = mg.Key.rec,
                                              INN = mg.Key.inn,
                                              Descr = mg.Key.charact,
                                              Manuf = mg.Key.manuf,
                                              Cost = Convert.ToDouble(mg.Sum(x => x.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : x.Field<double?>("G45 (Таможенная стоимость)")))
                                          }).ToList();

                            selData = (from s in preSelData
                                       group s by new
                                       {
                                           comp = s.Company
                                       } into sg
                                       where ((sg.Sum(x => x.Cost) / total) * 100) > maxCost
                                       select new SelectedData
                                       {
                                           Cost = sg.Sum(x => x.Cost),
                                           Company = sg.Key.comp
                                       }).ToList();

                            foreach (var predat in preSelData)
                            {
                                foreach (var dat in selData)
                                {
                                    if (predat.Company.Equals(dat.Company))
                                    {
                                        predat.Cost = dat.Cost;
                                        predat.Total = Convert.ToDouble(total);
                                        predat.Percent = String.Format("{0:0.##}", (predat.Cost / total) * 100) + "%";
                                        predat.Fields = new object[] { predat.Company, predat.INN, predat.Descr, predat.Manuf, predat.Cost, predat.Total, predat.Percent };
                                        finData.Add(predat);
                                    }
                                }
                            }
                        }
                        datas.Add(kvp.Key, finData);
                    }
                }
                catch (System.ArgumentException aex)
                {
                    log.Error("Table missformat \r\n {0}", aex.ToString());
                }
                catch (System.FormatException fex)
                {
                    log.Error("Failed to format double data from LINQ \r\n {0}", fex.ToString());
                }
                catch (System.InvalidCastException icex)
                {
                    log.Error("Failed to cast double data from LINQ \r\n {0}", icex.ToString());
                }
                finally { }
            }
            return datas;
        }

        private Dictionary<string, List<SelectedData>> Select(Dictionary<string, DataTable> data, int maxCost, string like)
        {
            Dictionary<string, List<SelectedData>> datas = new Dictionary<string, List<SelectedData>>();
            List<SelectedData> selData;
            List<SelectedData> preSelData;
            DataTable finData = init("Data"); ;

            foreach (KeyValuePair<string, DataTable> kvp in data)
            {
                try
                {
                    if (kvp.Value.Columns.Contains("G45 (Таможенная стоимость)"))
                    {
                        if (kvp.Value.Columns.Contains("Доля на рынке"))
                        {
                            IEnumerable<DataRow> query = (from row in kvp.Value.AsEnumerable()
                                       where (row.Field<string>("G31_1 (Описание и характеристика товара)") != null)
                                       && row.Field<string>("G31_1 (Описание и характеристика товара)").Contains(like)
                                       select finData.NewRow());

                            finData = query.CopyToDataTable<DataRow>();
                            DataTable da = finData;

                            /*foreach (var dat in finData)
                            {
                                dat.Fields = new object[] { dat.Company, dat.INN, dat.Descr, dat.Manuf, dat.Cost, dat.Total, dat.Percent };
                            }*/
                        }
                        else
                        {
                            var total = kvp.Value.AsEnumerable().Sum(t => t.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : t.Field<double?>("G45 (Таможенная стоимость)"));

                            preSelData = (from d in kvp.Value.AsEnumerable()
                                          group d by new
                                          {
                                              rec = d.Field<string>("G082 (Получатель)"),
                                              inn = (d.Field<double?>("G081 (ИНН получателя)") == null ? 0 : d.Field<double>("G081 (ИНН получателя)")),
                                              manuf = d.Field<string>("G31_11 (Фирма изготовитель)"),
                                              charact = d.Field<string>("G31_1 (Описание и характеристика товара)")
                                          } into mg
                                          where (mg.Key.charact != null)
                                          && mg.Key.charact.Contains(like)

                                          select new SelectedData
                                          {
                                              Company = mg.Key.rec,
                                              INN = mg.Key.inn,
                                              Descr = mg.Key.charact,
                                              Manuf = mg.Key.manuf,
                                              Cost = Convert.ToDouble(mg.Sum(x => x.Field<double?>("G45 (Таможенная стоимость)") == null ? 0 : x.Field<double?>("G45 (Таможенная стоимость)")))
                                          }).ToList();

                            selData = (from s in preSelData
                                       group s by new
                                       {
                                           comp = s.Company
                                       } into sg
                                       where ((sg.Sum(x => x.Cost) / total) * 100) > maxCost
                                       select new SelectedData
                                       {
                                           Cost = sg.Sum(x => x.Cost),
                                           Company = sg.Key.comp
                                       }).ToList();

                            foreach (var predat in preSelData)
                            {
                                foreach (var dat in selData)
                                {
                                    if (predat.Company.Equals(dat.Company))
                                    {
                                        predat.Cost = dat.Cost;
                                        predat.Total = Convert.ToDouble(total);
                                        predat.Percent = String.Format("{0:0.##}", (predat.Cost / total) * 100) + "%";
                                        predat.Fields = new object[] { predat.Company, predat.INN, predat.Descr, predat.Manuf, predat.Cost, predat.Total, predat.Percent };
                                        //finData.Add(predat);
                                    }
                                }
                            }
                        }
                        //datas.Add(kvp.Key, finData);
                    }
                }
                catch (System.ArgumentException aex)
                {
                    log.Error("Table missformat \r\n {0}", aex.ToString());
                }
                catch (System.FormatException fex)
                {
                    log.Error("Failed to format double data from LINQ \r\n {0}", fex.ToString());
                }
                catch (System.InvalidCastException icex)
                {
                    log.Error("Failed to cast double data from LINQ \r\n {0}", icex.ToString());
                }
                finally { }
            }
            return datas;
        }

        private DataTable init(string name)
        {
            DataTable dt = new DataTable(name);
            DataColumn col;

            col = new DataColumn();
            col.ColumnName = "G082 (Получатель)";
            col.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "G081 (ИНН получателя)";
            col.DataType = System.Type.GetType("System.Double");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "G31_1 (Описание и характеристика товара)";
            col.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "G31_11 (Фирма изготовитель)";
            col.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "G45 (Таможенная стоимость)";
            col.DataType = System.Type.GetType("System.Double");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "Объем рынка";
            col.DataType = System.Type.GetType("System.Double");
            dt.Columns.Add(col);

            col = new DataColumn();
            col.ColumnName = "Доля на рынке";
            col.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(col);
            
            return dt;
        }
        /// <summary>
        /// Selects data from Excel file
        /// </summary>
        /// <param name="data">raw data from file</param>
        /// <param name="maxCost">percent treshold</param>
        /// <param name="like">filter to include</param>
        /// <param name="notlike">filter to not include</param>
        /// <returns></returns>
        public Dictionary<string, List<SelectedData>> SelectData(Dictionary<string, DataTable> data, int maxCost, string like, string notlike)
        {
            if (String.IsNullOrEmpty(notlike))
            {
                return Select(data, maxCost, like);
            }
            else
            {
                return Select(data, maxCost, like, notlike);
            }
        }
    }
}
