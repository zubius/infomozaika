﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;
using System.Windows.Controls;
using NLog;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Saving selected data to file
    /// </summary>
    class SaveToXls
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        private readonly string[] cells = {"G082 (Получатель)", "G081 (ИНН получателя)", "G31_1 (Описание и характеристика товара)",
                                       "G31_11 (Фирма изготовитель)", "G45 (Таможенная стоимость)", "Объем рынка", "Доля на рынке"};

        /// <summary>
        /// Saving data to Excel file
        /// </summary>
        /// <param name="lst">data to save</param>
        /// <param name="filePath">path to file</param>
        /// <param name="sheet">name of sheet in workbook</param>
        public void SaveData(List<SelectedData> lst, string filePath, string sheet)
        {
            string newFile = FilePath(filePath) + FileName(filePath) + "_selected.xls";
            var missing = System.Reflection.Missing.Value;
            SelectedData dat = new SelectedData();

            Application app = new Application();
            if (app == null)
            {
                log.Error("EXCEL could not be started. Check that your office installation and project references are correct.");
                return;
            }
            //app.Visible = false;

            Workbook wbook = app.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            Worksheet wsheet = (Worksheet)wbook.Worksheets[1];
            wsheet.Name = sheet;
            if (wsheet == null)
            {
                log.Error("Worksheet could not be created. Check that your office installation and project references are correct.");
                return;
            }

            for (int i = 0; i < cells.Length; i++)
            {
                (wsheet.Cells[1, i + 1] as Range).Value2 = cells[i];
                for (int j = 0; j < lst.Count; j++)
                {
                    dat = lst[j];
                    if (dat[i] is double)
                    {
                        (wsheet.Cells[j + 2, i + 1] as Range).Value2 = (double)dat[i];
                    }
                    else
                    {
                        (wsheet.Cells[j + 2, i + 1] as Range).Value2 = (string)dat[i];
                    }
                }
            }



            wbook.SaveAs(@newFile, XlFileFormat.xlWorkbookNormal, missing, missing, false, false,
                XlSaveAsAccessMode.xlShared, false, false, missing, missing, missing);
            wbook.Close(missing, missing, missing);
            app.Quit();

        }

        private string FileName(string filePath)
        {
            string pattern = @"(?<=\\)([-0-9a-zA-Zа-яА-Я_ ]+).xls";
            Match m = Regex.Match(filePath, pattern);
            if (m.Success)
                return m.Groups[1].Value;
            else return null;
        }

        private string FilePath(string filePath)
        {
            string pattern = @".+\\(?=[-0-9a-zA-Zа-яА-Я_ ]+.xls)";
            Match m = Regex.Match(filePath, pattern);
            if (m.Success)
                return m.Value;
            else return null;
        }
    }
}
