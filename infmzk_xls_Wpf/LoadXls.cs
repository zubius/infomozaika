﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using NLog;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Loading data from Excel file
    /// </summary>
    class LoadXls
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        private string con = "";
        private string ext = "unknown";

        /// <summary>
        /// Creating connection for load data from Excel file
        /// </summary>
        /// <param name="uri">path to the file</param>
        public LoadXls(string uri)
        {
            if (uri.EndsWith(".xlsx"))
            {
                con = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                            "Data Source=" + @uri + ";" +
                            "Extended Properties=\"Excel 12.0 Xml;HDR=Yes\";";
                ext = ".xlsx";
            }
            else if (uri.EndsWith(".xls"))
            {
                con = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                            "Data Source=" + @uri + ";" +
                            "Extended Properties=\"Excel 8.0;HDR=Yes\";";
                ext = ".xls";
            }
        }

        /// <summary>
        /// Load data from Excel file
        /// </summary>
        /// <param name="sheets">list of sheets in file</param>
        /// <returns>raw data from the file</returns>
        public Dictionary<string, DataTable> GetData(List<string> sheets)
        {
            Dictionary<string, DataTable> data = new Dictionary<string, DataTable>();

            foreach (string sheet in sheets)
            {
                try
                {
                    OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + sheet + "$]", con);
                    DataTable t = new DataTable();
                    da.Fill(t);
                    data.Add(sheet, t);
                }
                catch (Exception ex)
                {
                    log.Error("Failed to open {0} file for data \r\n {1}", ext, ex.ToString());
                    data = null;
                }
                finally {}
            }
            return data;
        }
    }
}
