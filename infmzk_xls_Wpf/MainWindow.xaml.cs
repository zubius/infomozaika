﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;
using NLog;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filePath;
        private int maxCostThis = 30;
        private Dictionary<string, DataTable> data = new Dictionary<string, DataTable>();
        Dictionary<string, List<SelectedData>> datas;
        List<string> sheets;
        SelectXls slx = new SelectXls();
        /// <summary>
        /// Flag do not call filter data on texbox_TextChanged on initializing window
        /// </summary>
        bool isInitialized = false;

        string like = "";
        string notlike = "";

        private static Logger log = LogManager.GetCurrentClassLogger();

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Width;
            GridView.AutoGenerateColumns = true;
            maxCostTextBox.Text = maxCostThis.ToString();

            isInitialized = true;
            //log.Error("Window initialized fail");
        }

        private void Button_Select_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.DefaultExt = ".xls";
            openDialog.Filter = "Excel|*.xls;*.xlsx";

            Nullable<bool> result = openDialog.ShowDialog();

            if (result == true)
            {
                filePath = openDialog.FileName;
                pathtextbox.Text = filePath;

                LoadSheets ls = new LoadSheets(filePath);
                sheets = ls.sheets;
                LoadXls lxl = new LoadXls(filePath);
                data = lxl.GetData(sheets);

                PerformDataLoad(data, maxCostThis, like, notlike);
            }
            else
            {
                log.Error("openDialog returned false");
            }
        }

        private void maxCostTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                maxCostThis = Convert.ToInt32(maxCostTextBox.Text.ToString());
            }
            catch (System.FormatException sfex)
            {
                log.Error("Failed to convert maxCost to int64 \r\n {0}", sfex.ToString());
            }
            finally { }
        }

        private void GridView_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case "Company":
                    e.Column.Header = "Получатель";
                    break;
                case "INN":
                    e.Column.Header = "ИНН";
                    break;
                case "Descr":
                    e.Column.Header = "Описание товара";
                    break;
                case "Manuf":
                    e.Column.Header = "Производитель";
                    break;
                case "Cost":
                    e.Column.Header = "Сумма";
                    break;
                case "Total":
                    e.Column.Header = "Объем рынка";
                    break;
                case "Percent":
                    e.Column.Header = "Доля на рынке";
                    break;
                default:
                    break;
            }
        }

        private void reCalcButton_Click(object sender, RoutedEventArgs e)
        {
            PerformDataLoad(data, maxCostThis, like, notlike);
        }

        private void sheetComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = sheetComboBox.SelectedIndex;
            GridView.ItemsSource = datas.Values.ElementAt<List<SelectedData>>(i);
        }

        /// <summary>
        /// Load and filter data coresponding to filter boxes and treshold
        /// </summary>
        /// <param name="data">selected previously data</param>
        /// <param name="maxCost">percent treshold</param>
        /// <param name="like">data to include in filter</param>
        /// <param name="notlike">data to not include in filter</param>
        private void PerformDataLoad(Dictionary<string, DataTable> data, int maxCost, string like, string notlike)
        {
            if (data != null)
            {
                datas = slx.SelectData(data, maxCost, like, notlike);
                GridView.ItemsSource = datas.Values.First<List<SelectedData>>();
                sheetComboBox.ItemsSource = datas.Keys.ToList<string>();
                sheetComboBox.SelectedIndex = 0;

                FitToContent();
                //GridView.Columns[2].MaxWidth = GridView.he
            }
            else
            {
                log.Error("Cannot open file while perform dataload");
            }
        }

        private void notLikeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            notlike = notLikeTextBox.Text.ToUpper().Remove(notLikeTextBox.Text.Length - 2);
            if (isInitialized)//if (!String.IsNullOrEmpty(notlike))
            {
                PerformDataFilter(data, maxCostThis, like, notlike);
            }
        }

        private void likeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            like = likeTextBox.Text.ToUpper().Remove(likeTextBox.Text.Length - 2);
            if (!String.IsNullOrEmpty(like))
            {
                PerformDataFilter(data, maxCostThis, like, notlike);
            }
        }   

        /// <summary>
        /// Filter data by Lile and NotLike fields
        /// </summary>
        /// <param name="data">selected previously data</param>
        /// <param name="maxCost">percent treshold</param>
        /// <param name="like">data to include in filter</param>
        /// <param name="notlike">data to not include in filter</param>
        private void PerformDataFilter(Dictionary<string, DataTable> data, int maxCost, string like, string notlike)
        {
            if (data != null && sheetComboBox.Items.Count != 0)
            {
                datas = slx.SelectData(data, maxCost, like, notlike);
                if (datas.Keys.Count != sheetComboBox.Items.Count)
                {
                    sheetComboBox.ItemsSource = datas.Keys.ToList<string>();
                    sheetComboBox.SelectedIndex = 0;
                }
                GridView.ItemsSource = datas.Values.ElementAt<List<SelectedData>>(sheetComboBox.SelectedIndex);
                FitToContent();
            }
            else
            {
                log.Error("Cannot open file while perform datafilter");
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (datas != null)
            {
                SaveToXls stx = new SaveToXls();
                stx.SaveData(datas.Values.ElementAt(sheetComboBox.SelectedIndex), filePath, datas.Keys.ElementAt(sheetComboBox.SelectedIndex));
            }
        }

        /// <summary>
        /// Fix description column size to constant
        /// </summary>
        private void FitToContent()
        {
            GridView.Columns[2].Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToHeader);
            /*foreach (DataGridColumn column in GridView.Columns)
            {
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToHeader);
            }*/
        }
    }
}
