﻿using System;
using System.Collections;
using System.Text;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Class holds selected data
    /// </summary>
    class SelectedData
    {
        public string Company { get; set; }
        public double INN { get; set; }
        public string Descr { get; set; }
        public string Manuf { get; set; }
        public double Cost { get; set; }
        public double Total { get; set; }
        public string Percent { get; set; }

        public object[] Fields;
        public object this[int i] 
        {
            get 
            {
                return Fields[i];
            }
        }

        public SelectedData()
        {
            Fields = new object[]{Company, INN, Descr, Manuf, Cost, Total, Percent};
        }

        /// <summary>
        /// Convert all fields into one string for saving to Excel
        /// </summary>
        /// <returns>formatted string of fields for saving to Excel</returns>
        public string GetString()
        {
            Company = String.IsNullOrEmpty(Company) ? " " : Company;
            Descr = String.IsNullOrEmpty(Descr) ? " " : Descr;
            Manuf = String.IsNullOrEmpty(Manuf) ? " " : Manuf;
            Percent = String.IsNullOrEmpty(Percent) ? " " : Percent;

            StringBuilder sb = new StringBuilder();
            sb.Append("'" + Company.Replace("'", "") + "',");
            sb.Append("'" + INN + "',");
            sb.Append("'" + Descr.Replace("'", "") + "',");
            sb.Append("'" + Manuf.Replace("'", "") + "',");
            sb.Append("'" + Cost + "',");
            sb.Append("'" + Total + "',");
            sb.Append("'" + Percent.Replace("'", "") + "'");

            return sb.ToString();
        }
    }
}
