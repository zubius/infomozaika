﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Threading.Tasks;
using NLog;

namespace infmzk_xls_Wpf
{
    /// <summary>
    /// Loading list of sheets in file
    /// </summary>
    class LoadSheets
    {
        private OleDbConnection odcon = null;
        /// <summary>
        /// List of sheets
        /// </summary>
        public List<string> sheets = new List<string>();
        private static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Load list of sheets from Excel file
        /// </summary>
        /// <param name="uri">path to the file</param>
        public LoadSheets(string uri)
        {
            string con = null;
            string ext = "unknown";
            if (uri.EndsWith(".xlsx"))
            {
                con = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                            "Data Source=" + @uri + ";" +
                            "Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
                ext = ".xlsx";
            }
            else if (uri.EndsWith(".xls"))
            {
                con = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                            "Data Source=" + @uri + ";" +
                            "Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                ext = ".xls";
            }

            try
            {
                odcon = new OleDbConnection(con);
                odcon.Open();
                DataTable sdt = odcon.GetSchema("Tables");
                foreach (DataRow dr in sdt.Rows)
                {
                    string sheetName = (string)dr["TABLE_NAME"];
                    if (sheetName.EndsWith("$"))
                    {
                        sheetName = sheetName.Substring(0, sheetName.Length - 1);
                    }
                    else if (sheetName.EndsWith("'"))
                    {
                        sheetName = sheetName.Substring(1, sheetName.Length - 3);
                    }
                    sheets.Add(sheetName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to open {0} file for getting list names \r\n {1}", ext, ex.ToString());
            }
            finally
            {
                odcon.Close();
            }
        }
    }
}
